﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Education
    {
        public Universities universities;
        public int from;
        public int to;

        public Education(Universities universities, int from, int to)
        {
            this.universities = universities;
            this.from = from;
            this.to = to;
        }

        public void Result()
        {
            Console.WriteLine($"{universities} {from} {to}"); 
        }
    }
}
